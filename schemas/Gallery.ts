import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Gallery = list({
    access: {
      create: permissions.canManagePeople,
      read: true,
      update: rules.canUpdatePeople,
      delete: permissions.canManagePeople,
    },
    fields: {
        title: text(),
        author: relationship({
          ref: 'User.gallery'
        }),
        publishDate: timestamp(),
        picture: image({ 
            isRequired: true,
        }),
        image: relationship({
          ref:"Heacnew.picture"
        })

      }
});