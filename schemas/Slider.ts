import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Slider = list({
    fields: {
        title: text(),
        link: text(),
        author: relationship({
          ref: 'User.slide'
        }),
        sescription:text(),
        publishDate: timestamp(),
        picture: image({ isRequired: true })
      }
});