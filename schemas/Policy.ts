import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Policy = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true }),
        link: text(),
        description:text(),
        publishDate: timestamp(),
        picture:image(),
        icon:image(),
        policy:relationship({ref:"Programe.policy"})
      }
});