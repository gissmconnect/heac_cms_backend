import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,file } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const UpFooter = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true}),
        link1: text(),
        link2: text(),
        description:text(),
        publishDate: timestamp(),
        icon1:image(),
        icon2:image(),     
      }
});