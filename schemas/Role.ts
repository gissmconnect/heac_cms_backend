import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image, checkbox } from '@keystone-next/fields';
import { isSignedIn, permissions, rules } from '../access';

export const Role = list({
    access: {
        create: permissions.canManageRoles,
        read: isSignedIn,
        update: permissions.canManageRoles,
        delete: permissions.canManageRoles,
      },
      ui: {
        hideCreate: args => !permissions.canManageRoles(args),
        hideDelete: args => !permissions.canManageRoles(args),
        listView: {
          initialColumns: ['name', 'assignedTo'],
        },
        itemView: {
          defaultFieldMode: args => (permissions.canManageRoles(args) ? 'edit' : 'read'),
        },
      },
      fields: {
        name: text({ isRequired: true }),
        canCreateTodos: checkbox({ defaultValue: false }),
        canManageAllTodos: checkbox({ defaultValue: false }),
        canSeeOtherPeople: checkbox({ defaultValue: false }),
        canEditOtherPeople: checkbox({ defaultValue: false }),
        canManagePeople: checkbox({ defaultValue: false }),
        canManageRoles: checkbox({ defaultValue: false }),
        assignedTo: relationship({
          ref: 'User.role',
          many: true,
          ui: {
            itemView: { fieldMode: 'read' },
          },
        }),
      },
});