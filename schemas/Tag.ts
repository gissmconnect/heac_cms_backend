import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Tag = list({
    ui: {
        isHidden: true,
      },
      fields: {
        name: text(),
        posts: relationship({
          ref: 'Post.tags',
          many: true,
        }),
      },
});