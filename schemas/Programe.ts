import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Programe = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        title: text({ isRequired: true }),
        heading: text(),
        description:text(),
        author: relationship({
          ref: 'User.program',
          
        }),
        policy:relationship({ref:"Policy.policy"}),
        guidebook:relationship({ref:"Guide.guide"}),
        admissionsupport:relationship({ref:"Support.admissionsupport"}),
        admissiondate:relationship({ref:"Date.admissiondate",many:true}),

        publishDate: timestamp({label:"تاريخ النشر"}),
        picture:image({label:"صورة"})
      }
});