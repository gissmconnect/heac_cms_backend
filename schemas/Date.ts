import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image,file } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Date = list({
    access: {
        create: permissions.canManagePeople,
        read: true,
        update: rules.canUpdatePeople,
        delete: permissions.canManagePeople,
      },
      fields: {
        programeCode: text({ isRequired: true }),
        link: text(),
        description:text(),
        startDate:timestamp({isRequired:true}),
        endDate:timestamp({isRequired:true}),
        publishDate: timestamp(),
        picture:image(),
        icon:image(),
        admissiondate:relationship({ref:"Programe.admissiondate"})
      }
});