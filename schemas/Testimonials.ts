import { list } from '@keystone-next/keystone/schema';
import { text, password, relationship, select, timestamp, image, checkbox } from '@keystone-next/fields';
import { permissions, rules } from '../access';

export const Testimonial = list({
    access: {
      create: permissions.canManagePeople,
      read: true,
      update: rules.canUpdatePeople,
      delete: permissions.canManagePeople,
    },
    fields: {
        name: text({ isRequired: true }),
        description: text(),
        designation: text(),
        publishDate: timestamp(),
        picture:image(),
        createdAt:timestamp({defaultValue:new Date()}),
        isPublish:checkbox({
            defaultValue:false
        })
        
      }
});